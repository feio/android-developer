/*******************************************************************************
 * Copyright 2013 Federico Iosue (federico.iosue@gmail.com)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package it.feio.android.androiddeveloperconsole;

import java.util.List;

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class NavigationDrawerItemAdapter extends BaseAdapter {

	Context context;
	String[] mTexts, mUrls;
	TypedArray mIcons;
	LayoutInflater inflater;
	private List<NavigationDrawerItem> mNavigationDrawerItemList;

	public NavigationDrawerItemAdapter(Context context, List<NavigationDrawerItem> list) {
		this.context = context;
		mNavigationDrawerItemList = list;
	}

	@Override
	public int getCount() {
		return mNavigationDrawerItemList.size();
	}

	@Override
	public Object getItem(int position) {
		return mNavigationDrawerItemList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		
		// Retreive navigation item
		NavigationDrawerItem item = mNavigationDrawerItemList.get(position);

		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View itemView = inflater.inflate(R.layout.drawer_list_item, parent, false);
		boolean checked = ((ListView)parent).isItemChecked(position);

		// Locate the TextViews in drawer_list_item.xml
		TextView text = (TextView) itemView.findViewById(R.id.text);

		// Locate the ImageView in drawer_list_item.xml
		ImageView icon = (ImageView) itemView.findViewById(R.id.icon);

		// Set the results into TextViews	
		text.setText(item.getText());
		
		// Check the selected one
//		int selected = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(context).getString(Constants.PREF_NAVIGATION, "0"));
//		if (convertView != null && checked || position == selected)
//			text.setTextColor(context.getResources().getColor(android.R.color.tab_indicator_text));

		// Set the results into ImageView
		icon.setImageResource(item.getIcon());

		return itemView;
	}

}
