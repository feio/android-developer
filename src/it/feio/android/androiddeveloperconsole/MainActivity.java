package it.feio.android.androiddeveloperconsole;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ListView;

public class MainActivity extends Activity {

	private String[] navigation_texts, navigation_urls;
	TypedArray navigation_icons;
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private WebView myWebView;
	private ProgressDialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		dialog = new ProgressDialog(this);

		navigation_texts = getResources().getStringArray(
				R.array.navigation_texts);
		navigation_urls = getResources()
				.getStringArray(R.array.navigation_urls);
		navigation_icons = getResources().obtainTypedArray(
				R.array.navigation_icons);

		List<NavigationDrawerItem> list = new ArrayList<NavigationDrawerItem>();
		NavigationDrawerItem mNavigationDrawerItem;
		for (int i = 0; i < navigation_texts.length; i++) {
			mNavigationDrawerItem = new NavigationDrawerItem(
					navigation_texts[i], navigation_urls[i],
					navigation_icons.getIndex(i));
			list.add(mNavigationDrawerItem);
		}

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);

		// Set the adapter for the list view
		mDrawerList.setAdapter(new NavigationDrawerItemAdapter(this, list));
		// Set the list's click listener
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

		myWebView = (WebView) findViewById(R.id.webview);
		myWebView.setWebViewClient(new WebViewClient() {
			
			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				super.onPageStarted(view, url, favicon);
				dialog.show();
			}
			 
			@Override
			public void onPageFinished(WebView view, String url) {
				if (dialog.isShowing()) {
					dialog.dismiss();
				}
			}
		});
		myWebView.getSettings().setJavaScriptEnabled(true);
	}

	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView parent, View view, int position,
				long id) {
			NavigationDrawerItem mNavigationDrawerItem = (NavigationDrawerItem) mDrawerList
					.getAdapter().getItem(position);
			myWebView.loadUrl(mNavigationDrawerItem.getUrl());
			mDrawerLayout.closeDrawers();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
